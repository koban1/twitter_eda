Twitter Exploratory Data Analysis
================

The `twitter_eda` library is designed for end-to-end exploratory data
analysis starting with reading twitter jsons and ending with generation
of summary statistics and visualizaitons.

Twitter objects are extracted from twitter JSONs and analyzed using a
variety of methods.

## Minimal Example

``` r
import pandas as pd
import sys
sys.path.append(r'/Users/dankoban/Documents/twitter_analysis/chem_trials/')
import twitter_eda as tx
import webbrowser

# map to json files and divide into batches 
file_path = r'/Users/dankoban/Documents/rf_causes_cancer'
file_paths = tx.get_json_file_paths(file_path)
batches = list(tx.batch_files(file_paths, 4))

# initialize empty lists to store twitter-object data frames
user_dfs, message_dfs, geo_dfs = [[], [], []]
user_objs, message_objs, geo_objs = [[], [], []]

for batch in batches:
    # read json files
    tweet_dfs = tx.read_twitter_jsons(batch)

    for tweet_df in tweet_dfs:
        # extract 
        user_df, message_df, entity_df, extended_entity_df, geo_df = tx.extractDataFrames(tweet_df)
        
        user_dfs.append(user_df)
        geo_dfs.append(geo_df) 
        message_dfs.append(message_df)
        
        user_objs.append(tx.user_info(user_df))
        message_objs.append(tx.message_info(message_df))
        geo_objs.append(tx.geo_info(geo_df))

    # delete tweet data frames to free up memory
    del(tweet_dfs, message_df, tweet_df, geo_df, user_df, entity_df, extended_entity_df)
    
# Inspect user information and activity for all files
len(pd.concat(user_dfs))
all_users = tx.user_info(pd.concat(user_dfs))
all_users.get_user_name_counts()
all_users.get_user_name_counts()
all_users.get_most_active_ids()
all_users.get_most_active_names()
all_users.plot_posts_v_followers()

# Inspect tweet activity for all files
all_messages = tx.message_info(pd.concat(message_dfs))
all_messages.plot_tweets_by_day()

# Inspect geo-tagged tweets for all data
len(pd.concat(geo_dfs))
all_geos = tx.geo_info(pd.concat(geo_dfs))
my_map = all_geos.create_point_map()
my_map.save("map.html")
webbrowser.get('open -a /Applications/Google\ Chrome.app %s').open_new_tab('map.html')
```

![alt text](folium_map.png)

``` r
all_geos.create_choropleth_map()
```

![alt text](plotly_map.png)
