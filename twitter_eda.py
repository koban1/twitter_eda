import os
import pandas as pd
import geopandas as gpd
from concurrent.futures import ProcessPoolExecutor
from tqdm import tqdm
import matplotlib.pyplot as plt
import folium
import plotly.io as pio
import plotly.graph_objects as go
import plotly.express as px
import datetime
pio.renderers.default='browser'

#%% define functions
def get_json_file_paths(file_path):
    files = os.listdir(file_path)
    files = [file for file in files if '.json' in file]
    file_paths = [file_path + "/" + file for file in files]
    return(file_paths)

def batch_files(lst, n):
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i:i + n]        

def read_twitter_json(file_path):
    df = pd.read_json(file_path, lines = True)
    return(df)

def read_twitter_jsons(file_paths):
    with ProcessPoolExecutor() as pool:
        with tqdm(total = len(file_paths)) as progress:
            futures = []
            for file in file_paths:
                future = pool.submit(read_twitter_json, file)
                future.add_done_callback(lambda p: progress.update())
                futures.append(future)
            results = []
            for future in futures:
                result = future.result()
                results.append(result)
            return(results)
                
def extractDataFrames(tweet_df):
    user_df = pd.DataFrame({'id': tweet_df['user'].apply(lambda x: x['id']).tolist(),
                            'name': tweet_df['user'].apply(lambda x: x['name']).tolist(),
                            'screen_names': tweet_df['user'].apply(lambda x: x['screen_name']).tolist(),
                            'followers_count': tweet_df['user'].apply(lambda x: x['followers_count']).tolist(),
                            'friends_count': tweet_df['user'].apply(lambda x: x['friends_count']).tolist(),
                            'statuses_count': tweet_df['user'].apply(lambda x: x['statuses_count']).tolist()})
    
    message_df = pd.DataFrame({'id': tweet_df['id'].tolist(),
                               'created_at': tweet_df['created_at'].tolist(),
                               'text': tweet_df['full_text'].tolist()})
    
    entity_df = pd.DataFrame({'hashtags': tweet_df['entities'].apply(lambda x: x['hashtags']).tolist(),
                              'urls': tweet_df['entities'].apply(lambda x: x['urls']).tolist(),
                              'user_mentions': tweet_df['entities'].apply(lambda x: x['user_mentions']).tolist(),
                              'symbols': tweet_df['entities'].apply(lambda x: x['symbols']).tolist()})
    entity_df = entity_df[(entity_df['hashtags'].apply(lambda x: len(x)) > 0) |
                          (entity_df['urls'].apply(lambda x: len(x)) > 0) | 
                          (entity_df['user_mentions'].apply(lambda x: len(x)) > 0) |
                          (entity_df['symbols'].apply(lambda x: len(x)) > 0)]
    
    extended_entity_df = tweet_df[tweet_df['extended_entities'].isnull() == False]
    extended_entity_df = pd.DataFrame({'media': extended_entity_df['extended_entities'].apply(lambda x: x['media']).tolist()})
    
    geo_df = tweet_df[(tweet_df['place'].notnull()) & (tweet_df['coordinates'].notnull())]
        
    geo_df = pd.DataFrame({'lon': geo_df['coordinates'][geo_df['coordinates'].notnull()].apply(lambda x: x['coordinates']).apply(lambda x: x[0]),
                           'lat': geo_df['coordinates'][geo_df['coordinates'].notnull()].apply(lambda x: x['coordinates']).apply(lambda x: x[1]),
                           'place': geo_df['place'][geo_df['coordinates'].notnull()].apply(lambda x: x['full_name']),
                           'tweet_text': geo_df['full_text'][geo_df['coordinates'].notnull()],
                           'user_name': geo_df['user'][geo_df['coordinates'].notnull()].apply(lambda x: x['name'])})
        
    return(user_df, message_df, entity_df, extended_entity_df, geo_df)  

#%% User class
class user_info:
    def __init__(self, user_df):
        self.user_df = user_df
        self.id = user_df['id'].tolist()
        self.statuses_count = user_df['statuses_count'].tolist()
        self.followers_count = user_df['followers_count'].tolist()
        self.user_id_count = len(user_df['id'].unique().tolist())
        self.user_name_count = len(user_df['name'].unique().tolist())
        self.screen_name_count = len(user_df['screen_names'].unique().tolist())

    def plot_posts_v_followers(self):
        plt.scatter(self.statuses_count, self.followers_count,c='b',marker='o')
        plt.xlabel('Post Count', fontsize=16)
        plt.ylabel('Follower Count', fontsize=16)
        plt.title('Follower Count vs. User Activity',fontsize=20)     
        
    def get_user_name_counts(self):
        user_stats = pd.DataFrame({'user_id_count': self.user_id_count,
                                   'user_name_count': self.user_name_count,
                                   'user_screen_name_count': self.screen_name_count}, index = [0])
        return(user_stats)
    
    def get_most_active_ids(self):
        message_count_by_id = self.user_df['id'].value_counts().reset_index()
        message_count_by_id.columns = ['user_id', 'post_count']
        return(message_count_by_id)
    
    def get_most_active_names(self):
        message_count_by_name = self.user_df['name'].value_counts().reset_index()
        message_count_by_name.columns = ['user_name', 'post_count']
        return(message_count_by_name)

#%% Message class
class message_info:
    def __init__(self, message_df):
        self.message_df = message_df
        
    def plot_tweets_by_day(self):
        self.message_df['date'] = self.message_df['created_at'].apply(lambda x: datetime.datetime.date(x))
        tweets_by_day = self.message_df['date'].value_counts().reset_index().sort_values(by = 'index')
        tweets_by_day.columns = ['Date', 'Count']
        fig = px.bar(tweets_by_day, x='Date', y='Count')
        return(fig)

#%% Geo class
class geo_info:
    def __init__(self, geo_df):
        self.geo_df = geo_df
        self.lon = geo_df['lon'].tolist()
        self.lat = geo_df['lat'].tolist()
        self.place = geo_df['place'].tolist()
        
    def get_location_tweet_count(self):
        location_tweet_count = self.geo_df['place'].value_counts().reset_index()
        location_tweet_count.columns = ['place', 'tweet_count']
        return(location_tweet_count)
    
    def create_point_map(self):
        url_base = 'https://server.arcgisonline.com/ArcGIS/rest/services/'
        #service = 'World_Imagery/MapServer/tile/{z}/{y}/{x}'
        service = 'World_Street_Map/MapServer'
        attribution = 'ESRI'
        tileset = url_base + service

        map_center = [20, 0]
        my_map = folium.Map(location=(map_center[0], map_center[1]), tiles = None, zoom_start = 3)
        folium.TileLayer("cartodbdark_matter", name = "Dark Matter").add_to(my_map)
        folium.TileLayer(tileset, attr = attribution, name = "World Imagery").add_to(my_map)
        self.geo_df['popup_txt'] = "<b>User: </b>" + self.geo_df['user_name'] + "<br>" + \
                                   "<b>Location: </b>" + self.geo_df['place'] + "<br>" + \
                                   "<b>Tweet: </b>" + self.geo_df['tweet_text']
    
        for i in range(0,len(self.geo_df)):
            iframe = folium.IFrame(html = self.geo_df.iloc[i]['popup_txt'], width = 400, height = 120)
            popup = folium.Popup(iframe, max_width = 400, parse_html = True)
            folium.CircleMarker([self.geo_df.iloc[i]['lat'], self.geo_df.iloc[i]['lon']], 
                                 popup=popup, color = "blue", radius = 2).add_to(my_map)
        return(my_map)

    def create_choropleth_map(self):
        
        # Read in shapefiles and convert geo_df coordinates to point geometries
        #'https://www2.census.gov/geo/tiger/GENZ2018/shp/cb_2018_us_state_500k.zip'
        us_states = gpd.read_file(r'/Users/dankoban/Documents/twitter_analysis/chem_trials/cb_2018_us_state_500k')
        gdf = gpd.GeoDataFrame(self.geo_df, geometry=gpd.points_from_xy(self.geo_df.lon, self.geo_df.lat))

        # geocode point geometries by counting points that fall within the state polygons
        df = []
        for value in range(0,len(us_states)):    
            temp = gdf[gdf.geometry.within(us_states.geometry[value])]
            temp = temp.assign(code=us_states.STUSPS[value])
            df.append(temp)
        df = pd.concat(df)
        
        # summarize the state counts
        count_df = pd.DataFrame({'state_name': df['code'].tolist()})
        count_df = count_df['state_name'].value_counts().reset_index()
        count_df.columns = ['state_name', 'count']

        fig = go.Figure(data=go.Choropleth(
                        locations=count_df['state_name'], # Spatial coordinates
                        z = count_df['count'].astype(float), # Data to be color-coded
                        locationmode = 'USA-states', # set of locations match entries in `locations`
                        colorscale = 'Reds',
                        colorbar_title = "Tweet Counts",
                        ))

        fig.update_layout(title_text = 'Tweets by State',
                          geo_scope='usa', # limite map scope to USA
                          )

        return(fig)

